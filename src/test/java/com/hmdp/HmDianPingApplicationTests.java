package com.hmdp;

import com.hmdp.utils.RedisIdWorker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = HmDianPingApplication.class)
@RunWith(SpringRunner.class)
public class HmDianPingApplicationTests {

    @Resource
    private RedisIdWorker redisIdWorker;

    @Test
    public void testId(){
        long order = redisIdWorker.nextId("order");
        System.out.println(order);
    }
}
