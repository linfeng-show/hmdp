package com.hmdp.Interceptor;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.hmdp.dto.UserDTO;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.LOGIN_USER_KEY;
import static com.hmdp.utils.RedisConstants.LOGIN_USER_TTL;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Slf4j
public class RefreshTokenInterceptor implements HandlerInterceptor {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 如果请求不是映射到方法直接通过
        if(!(handler instanceof HandlerMethod)){
            return true;
        }
////        获取session
//        HttpSession session = request.getSession();
////        从session获得用户
//        Object user = session.getAttribute("user");

//        获取token
        String token = request.getHeader("authorization");

        if(StrUtil.isBlank(token)){
//            token不存在，说明未登录
//            throw new RuntimeException("未携带token");
            return true;
        }
//        基于token，获取redis中的数据
        String key = LOGIN_USER_KEY + token;
        Map<Object, Object> user = redisTemplate.opsForHash().entries(key);

        if(user.isEmpty()){
//            不存在，直接拦截
//            throw new RuntimeException("请先登录！");
            return true;
        }else{
//            转换map为对象，然后存储
            UserDTO userDTO = BeanUtil.fillBeanWithMap(user, new UserDTO(), false);
            //        存在则保存到用户进程
            log.debug(userDTO.toString());
            UserHolder.saveUser(userDTO);
//        重置有效期
            redisTemplate.expire(key,LOGIN_USER_TTL, TimeUnit.MINUTES);
            return true;
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserHolder.removeUser();
    }
}
