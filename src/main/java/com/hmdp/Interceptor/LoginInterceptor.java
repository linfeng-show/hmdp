package com.hmdp.Interceptor;


import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {


//
//    public LoginInterceptor(StringRedisTemplate redisTemplate) {
//        this.redisTemplate = redisTemplate;
//    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 如果请求不是映射到方法直接通过
        if(!(handler instanceof HandlerMethod)){
            return true;
        }

        if(UserHolder.getUser() == null){
            throw new RuntimeException("请先登录");
        }else{
            return true;
        }

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        UserHolder.removeUser();

    }
}
