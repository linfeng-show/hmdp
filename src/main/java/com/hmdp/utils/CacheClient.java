package com.hmdp.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.hmdp.utils.RedisConstants.LOCK_SHOP_TTL;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 * 封装redis工具类
 */
@Component
@Slf4j
public class CacheClient {

    private final StringRedisTemplate stringRedisTemplate;

    public CacheClient(StringRedisTemplate redisTemplate) {
        this.stringRedisTemplate = redisTemplate;
    }


    public void set(String key, Object value, Long time, TimeUnit unit){
        /**
        * @Description: 封装缓存存储工具
        * @Params: [key, value, time, unit]
         * 键，值，时间，时间单位
        * @Return void
        */
        String data = JSONUtil.toJsonStr(value);

        stringRedisTemplate.opsForValue().set(key,data,time,unit);

    }

    public void setWithLogicalExpire(String key,Object value,Long time,TimeUnit unit){
        /**
        * @Description: 利用逻辑时间存储，防止缓存击穿
        * @Params: [key, value, time, unit]
         * 键，值，时间，时间单位
        * @Return void
        */

//        这里存放的value应该是同包下RedisData类
        RedisData data = new RedisData();
        data.setData(value);
        data.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));

        String jsonStr = JSONUtil.toJsonStr(data);
        stringRedisTemplate.opsForValue().set(key,jsonStr);
    }

    public <R,ID> R queryWithPassThrough(String keyPrefix, ID id, Class<R> type ,Function<ID,R> dbFallback,Long time,TimeUnit unit){
        /**
        * @Description: 读数据并防止缓存穿透
        * @Params: [key, id, type, dbFallback,time,unit]
         * 键，查询id，转换类型，数据库查询方法，过期时间，时间类型
        * @Return R
        */
        String key = keyPrefix + id;

        String json = stringRedisTemplate.opsForValue().get(key);
        if(StrUtil.isNotBlank(json)){
//            命中，则直接返回
            R bean = JSONUtil.toBean(json, type);
            return bean;
        }
        if(json != null){
//            命中空数据
            return null;
        }
//        没命中则查询数据库
        R r = dbFallback.apply(id);
        if(r == null){
//            存入空数据
            stringRedisTemplate.opsForValue().set(key,"",RedisConstants.CACHE_NULL_TTL,TimeUnit.MINUTES);
            throw new RuntimeException("未命中，内存存入空数据");
        }
//        存入缓存
        stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(r),time,unit);
        return r;
    }

    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    public <R,ID> R queryWithLogicalExpire(String keyPrefix, ID id, Class<R> type ,Function<ID,R> dbFallback,Long time,TimeUnit unit){
        /**
         * @Description: 读数据并防止缓存击穿
         * @Params: [key, id, type, dbFallback,time,unit]
         * 键，查询id，转换类型，数据库查询方法，过期时间，时间类型
         * @Return R
         */
        String key = keyPrefix + id;
        String json = stringRedisTemplate.opsForValue().get(key);
        if(StrUtil.isBlank(json)){
            return null;
        }

//        命中，则转换
        RedisData redisData = JSONUtil.toBean(json, RedisData.class);
        Object data = redisData.getData();
        R r = JSONUtil.toBean((JSONObject) data, type);
        LocalDateTime expireTime = redisData.getExpireTime();
        if(expireTime.isAfter(LocalDateTime.now())){
//            未过期，直接返回
            return  r;
        }
//        过期了，则调用线程，更新缓存
        String lockKey = RedisConstants.LOCK_SHOP_KEY + id;
        boolean isLock = tryLock(lockKey);

        if(isLock){

//            二次验证,防止重复重建
//            json = redisTemplate.opsForValue().get(key);
//            if(StrUtil.isBlank(json)){
//                return null;
//            }
//
////        命中，则转换
//            redisData = JSONUtil.toBean(json, RedisData.class);
//            data = redisData.getData();
//            R reR = JSONUtil.toBean((JSONObject) data, type);
//            expireTime = redisData.getExpireTime();
//            if(expireTime.isAfter(LocalDateTime.now())){
////            未过期，直接返回
//                return  reR;
//            }

//            验证失败，申请线程，重建缓存
            CACHE_REBUILD_EXECUTOR.submit(() ->{
                try{
//                    重建缓存
//                    查询数据库
                    R r1 = dbFallback.apply(id);
//                    写入缓存
                    this.setWithLogicalExpire(key,r1,time,unit);
                }catch (Exception e){
                    throw new RuntimeException(e);
                }finally{
//                    释放锁
                    unLock(key);
                }
            });

        }
//        返回旧数据
        return r;
    }


    private boolean tryLock(String key){
        Boolean b = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", LOCK_SHOP_TTL, TimeUnit.SECONDS);

        return BooleanUtil.isTrue(b);
    }

    //    释放互斥锁
    private void unLock(String key){
        stringRedisTemplate.delete(key);
    }

}
