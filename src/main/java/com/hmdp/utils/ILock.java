package com.hmdp.utils;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public interface ILock {
    /**
    * @Description: 此乃获取分布式锁的接口
    * @Params:
    * @Return
    */

//    获取锁
    boolean tryLock(long timeSec);

//    释放锁
    void unLock();
}
