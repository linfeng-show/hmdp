package com.hmdp.utils;


import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Component
public class RedisIdWorker {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
//  开始时间戳
    private static final long BEGIN_TIMESTAMP = 1640995200L;

//    序列号位数
    private static final int COUNT_BITS = 32;



    public long nextId(String keyPrefix){
//        生成时间戳
        LocalDateTime now = LocalDateTime.now();
        long nowSecond = now.toEpochSecond(ZoneOffset.UTC);
        long timestamp = nowSecond - BEGIN_TIMESTAMP;
//        生成序列号
//          获取当前日期，作为序列号的一部分
        String date = now.format(DateTimeFormatter.ofPattern("yyyy:MM:dd"));
//          自增长
        Long count = stringRedisTemplate.opsForValue().increment("icr:" + keyPrefix + ":" + date);
//        拼接返回
//        采用时间戳左移序列号位数，然后用序列号和时间戳或运算
        return timestamp << COUNT_BITS | count;
    }


}
