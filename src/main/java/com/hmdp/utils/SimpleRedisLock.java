package com.hmdp.utils;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.BooleanUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
public class SimpleRedisLock implements ILock {


    private StringRedisTemplate stringRedisTemplate;
    //    锁前缀

    private static final String LOCK_PREFIX = "Lock:";
    //    锁名
    private String name;

//    线程标识
    private static final String ID_PREFIX = UUID.randomUUID().toString(true);

//    redis脚本
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;
    static{
//        静态代码块，随类一起加载执行,防止浪费IO资源
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("delLock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    public SimpleRedisLock(StringRedisTemplate stringRedisTemplate, String name) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.name = name;
    }

    @Override
    public boolean tryLock(long timeSec) {
//      获取当前进程id
        String id = ID_PREFIX + Thread.currentThread().getId();
        Boolean success = stringRedisTemplate.opsForValue().setIfAbsent(LOCK_PREFIX + name, id, timeSec, TimeUnit.SECONDS);

        return BooleanUtil.isTrue(success);
    }

    @Override
    public void unLock() {

//        调用redis脚本
        stringRedisTemplate.execute(
                /**
                * @Description: 执行redis脚本
                * @Params: [脚本对象，keys（Collection），args]
                * @Return void
                */
                UNLOCK_SCRIPT,
                Collections.singletonList(LOCK_PREFIX + name),
                ID_PREFIX + Thread.currentThread().getId()
        );
        //      获取当前进程id
//        String id = ID_PREFIX + Thread.currentThread().getId();
//        String s = stringRedisTemplate.opsForValue().get(LOCK_PREFIX + name);
//        if(id.equals(s)){
//            stringRedisTemplate.delete(LOCK_PREFIX + name);
//        }

    }
}
