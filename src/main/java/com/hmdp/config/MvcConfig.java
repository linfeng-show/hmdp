package com.hmdp.config;

import com.hmdp.Interceptor.LoginInterceptor;
import com.hmdp.Interceptor.RefreshTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author 林峰
 * @Date
 * @Version 1.0
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationLoginInterceptor())
                .excludePathPatterns(
                    "/user/code",
                        "/user/login",
                        "/blog/hot",
                        "/shop/**",
                        "shop-type/**",
                        "/upload/**",
                        "/voucher/**"
                ).order(1);
        registry.addInterceptor(authenticationRefreshInterceptor())
                .order(0);
    }

    @Bean
    public LoginInterceptor authenticationLoginInterceptor() {
        return new LoginInterceptor();
    }

    @Bean
    public RefreshTokenInterceptor authenticationRefreshInterceptor(){
        return new RefreshTokenInterceptor();
    }
}
