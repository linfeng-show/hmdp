package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.LoginFormDTO;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.User;
import com.hmdp.mapper.UserMapper;
import com.hmdp.service.IUserService;
import com.hmdp.utils.RegexUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.*;
import static com.hmdp.utils.SystemConstants.USER_NICK_NAME_PREFIX;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {



    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public Result login(LoginFormDTO loginForm, HttpSession session) {

        /**
        * @Description:
        * @Params: [loginForm, session]
         * 前端传入的登录信息，还有服务端保存的session
        * @Return java.lang.String
         * 返回信息
        */
////        获取session并校验手机号
//        String phone = loginForm.getPhone();
//        //        获取session中的code
//        Object code = session.getAttribute(phone);
//       获取手机号
        String phone = loginForm.getPhone();
        String code = redisTemplate.opsForValue().get(LOGIN_CODE_KEY + phone);

//        通过radis获取code
        if(code == null){
            return Result.fail("手机号不一致");
        }
//        获取前端输入的验证码
        String inCode = loginForm.getCode();

        if(!inCode.equals(code)){
            return Result.fail("验证码有误");
        }
//        判断用户是否存在
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getPhone,phone);
        User one = getOne(queryWrapper);

        if(one == null){
//            不存在则创建用户
            User user = new User();
            user.setPhone(phone);
//            设置昵称
            user.setNickName(USER_NICK_NAME_PREFIX + RandomUtil.randomString(10));
            save(user);

        }else{
//            do nothing
        }
        //            保存session
        User u = getOne(queryWrapper);

//        UserDTO userDTO = new UserDTO(u.getId(), u.getNickName(), u.getIcon());
        UserDTO userDTO = BeanUtil.copyProperties(u,UserDTO.class);
//        session.setAttribute("user",userDTO);

//        生成随机token，作为令牌，还有redis中的key
        String token = UUID.randomUUID().toString(true);
        String tokenkey = LOGIN_USER_KEY + token; //也使用常量拼装的方式

//        将对象转为map格式
        Map<String, Object> map = BeanUtil.beanToMap(userDTO);
//      将map中的值全变string,否则在使用stringredis存储时会报类型转换错误
        map.forEach((key, value) -> {
            if (null != value) map.put(key, String.valueOf(value));
        });
//        存储到redis
        redisTemplate.opsForHash().putAll(tokenkey,map);
//        设置有效期
        redisTemplate.expire(tokenkey,LOGIN_USER_TTL, TimeUnit.MINUTES);

//        token返回前端，作为令牌
        return Result.ok(token);
    }

    @Override
    public String getCode(String phone) {

        /**
        * @Description:
        * @Params: [phone]
         * 登录的手机号
        * @Return java.lang.String
         * 生成的验证码
        */
//        调用hutool的工具，随机生成6位验证码
        String code  = RandomUtil.randomNumbers(6);

        return code;
    }
}
