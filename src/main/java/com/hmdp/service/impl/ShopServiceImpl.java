package com.hmdp.service.impl;

import com.hmdp.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.CacheClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private CacheClient cacheClient;

    @Override
    public Shop queryById(Long id) {
//        Shop shop = hcct(id);
//        Shop shop = hcjc(id);

//        引入自己封装的redis工具类
        Shop shop = cacheClient
                .queryWithPassThrough(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.MINUTES);
        return shop;

    }

//    public Shop hcjc(Long id){
//
//        String key = CACHE_SHOP_KEY + id;
//        String lock_key = LOCK_SHOP_KEY + id;
//        boolean t = true;
//
//        try{
//            while(t){
////      死循环等待
////        查询店铺缓存
//                Map<Object, Object> map = redisTemplate.opsForHash().entries(key);
//
////            if(map == null){
////                throw new RuntimeException("命中空数据");
////            }
//
//
//                if(!map.isEmpty()){
//                    //        命中则直接返回
//                    Shop shop = BeanUtil.fillBeanWithMap(map, new Shop(), false);
//                    return shop;
//                }
////        未命中，则获取互斥锁
//
//                boolean tag = tryLock(lock_key);
//
//                if(!tag){
////            获取互斥锁失败，休眠一段时间，循环等待
//                    Thread.sleep(50);
//                }else{
//                    t = false;
//                }
//            }
////        二次验证
//            //        查询店铺缓存
//            Map<Object, Object> map = redisTemplate.opsForHash().entries(key);
//
////        if(map.isEmpty()){
////            throw new RuntimeException("命中空数据");
////        }
//
//            if(!map.isEmpty()){
//                //        命中则直接返回
//                Shop shop = BeanUtil.fillBeanWithMap(map, new Shop(), false);
//                return shop;
//            }
//
//            Shop shop = getById(id);
//
//            if(shop ==  null){
//                //        数据在数据库中不存在，则返回错误
//                redisTemplate.opsForValue().set(key,null,CACHE_NULL_TTL,TimeUnit.MINUTES);
//
//                throw new RuntimeException("店铺信息有误");
//            }
////        将数据保存到缓存
//            Map<String, Object> mapshop = BeanUtil.beanToMap(shop);
//            mapshop.forEach((k,value) ->{
//                if (null != value) mapshop.put(k, String.valueOf(value));
//            });
//            redisTemplate.opsForHash().putAll(key,mapshop);
//            redisTemplate.expire(key,CACHE_SHOP_TTL, TimeUnit.MINUTES);
////        返回
//            return shop;
//        }catch (Exception e){
//           throw new RuntimeException(e);
//        }finally {
//            unLock(lock_key);
//        }
//
//    }

//    public Shop hcct(Long id){
////        引入自己封装的redis工具类
//
//        Shop shop = redisClient
//                .queryWithPassThrough(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.MINUTES);
//        return shop;


//        String key = CACHE_SHOP_KEY + id;
////        查询店铺缓存
//        Map<Object, Object> map = redisTemplate.opsForHash().entries(key);
//
//        if(map == null){
//            throw new RuntimeException("命中空数据");
//        }
//
//        if(!map.isEmpty()){
//            //        命中则直接返回
//            Shop shop = BeanUtil.fillBeanWithMap(map, new Shop(), false);
//            return shop;
//        }
////        未命中，则查询数据库
//        Shop shop = getById(id);
//
//        if(shop ==  null){
//            //        数据在数据库中不存在，则返回错误
//            redisTemplate.opsForValue().set(key,null,CACHE_NULL_TTL,TimeUnit.MINUTES);
//            throw new RuntimeException("店铺信息有误");
//        }
////        将数据保存到缓存
//        Map<String, Object> mapshop = BeanUtil.beanToMap(shop);
//        mapshop.forEach((k,value) ->{
//            if (null != value) mapshop.put(k, String.valueOf(value));
//        });
//        redisTemplate.opsForHash().putAll(key,mapshop);
//        redisTemplate.expire(key,CACHE_SHOP_TTL, TimeUnit.MINUTES);
////        返回
//        return shop;
//    }

//    获取互斥锁


    @Override
    @Transactional
    public void update(Shop shop) {

        updateById(shop);
        Long id = shop.getId();

        stringRedisTemplate.delete(CACHE_SHOP_KEY + id);

    }
}
