package com.hmdp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Follow;
import com.hmdp.mapper.FollowMapper;
import com.hmdp.service.IFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.UserHolder;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class FollowServiceImpl extends ServiceImpl<FollowMapper, Follow> implements IFollowService {

    @Override
    public void follow(Long followed, boolean isFollow) {
        UserDTO user = UserHolder.getUser();
        if(user == null){
            throw new RuntimeException("请先登录！");
        }
        Long userId = user.getId();
//        关注
        if(isFollow){
            Follow follow = new Follow();
            follow.setUserId(userId);
            follow.setFollowUserId(followed);
//            保存
            save(follow);
        }else{
//            取关
            QueryWrapper<Follow> followQueryWrapper = new QueryWrapper<>();
            followQueryWrapper.eq("user_id",userId);
            followQueryWrapper.eq("follow_user_id",followed);

            remove(followQueryWrapper);
        }
    }

    @Override
    public boolean isFollow(Long writerId) {
        UserDTO user = UserHolder.getUser();
        if(user == null){
            return false;
        }
        Long userId = user.getId();

        QueryWrapper<Follow> followQueryWrapper = new QueryWrapper<>();
        followQueryWrapper.eq("user_id",userId);
        followQueryWrapper.eq("follow_user_id",writerId);

        Follow one = getOne(followQueryWrapper);
        return one != null;
    }
}
