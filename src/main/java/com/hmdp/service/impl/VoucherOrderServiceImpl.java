package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.SeckillVoucher;
import com.hmdp.entity.VoucherOrder;
import com.hmdp.mapper.VoucherOrderMapper;
import com.hmdp.service.ISeckillVoucherService;
import com.hmdp.service.IVoucherOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.service.IVoucherService;
import com.hmdp.utils.RedisIdWorker;
import com.hmdp.utils.SimpleRedisLock;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.aop.framework.AopContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static com.hmdp.utils.RedisConstants.LOCK_Order_KEY;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {

    @Resource
    private ISeckillVoucherService iSeckillVoucherService;

    @Resource
    private RedisIdWorker redisIdWorker;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedissonClient redissonClient;

//    @Resource
//    private BlockingQueue<VoucherOrder> ordersTask = new ArrayBlockingQueue<>(1024 * 1024);

//    线程池
    private static final ExecutorService SECKILL_ORDER_EXECUTOR = Executors.newSingleThreadExecutor();

    public IVoucherOrderService o;

    @PostConstruct
    private void init(){
//        log.info("1131");
        SECKILL_ORDER_EXECUTOR.submit(new VoucherOrderHandler());
    }

//    订单处理线程
    public class VoucherOrderHandler implements Runnable{

        private String queueName = "stream.orders";
        @Override
        public void run() {

            while(true){
                try{
//                    log.info("正在监听！1");
//                    获取消息队列中的订单信息
                    List<MapRecord<String, Object, Object>> read = stringRedisTemplate.opsForStream().read(
                            Consumer.from("g1", "c1"),
                            StreamReadOptions.empty().count(1).block(Duration.ofSeconds(2)),
                            StreamOffset.create(queueName, ReadOffset.lastConsumed())
                    );
//                    判断是否获取成功
                    if(read == null || read.isEmpty()){
                        //                    获取失败，说明没待处理消息了，继续下次循环
                        continue;
                    }
                    log.info("获取成功！");

//                    获取成功执行下单业务
                    MapRecord<String, Object, Object> record = read.get(0);
                    Map<Object, Object> value = record.getValue();
                    VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(value, new VoucherOrder(), true);
                    //                    下单
                    handleVoucherOrder(voucherOrder);
//                    ACK确认
                    stringRedisTemplate.opsForStream().acknowledge(queueName,"g1",record.getId());
                }catch (Exception e){
                    handlePendingList();
                    throw new RuntimeException("处理错误订单");
                }
            }
        }
    }

    private void handlePendingList() {
        while (true) {
            try {
                // 1.获取pending-list中的订单信息 XREADGROUP GROUP g1 c1 COUNT 1 BLOCK 2000 STREAMS s1 0
                List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
                        Consumer.from("g1", "c1"),
                        StreamReadOptions.empty().count(1),
                        StreamOffset.create("stream.orders", ReadOffset.from("0"))
                );
                // 2.判断订单信息是否为空
                if (list == null || list.isEmpty()) {
                    // 如果为null，说明没有异常消息，结束循环
                    break;
                }
                // 解析数据
                MapRecord<String, Object, Object> record = list.get(0);
                Map<Object, Object> value = record.getValue();
                VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(value, new VoucherOrder(), true);
                // 3.创建订单
                createVoucherOrder(voucherOrder);
                // 4.确认消息 XACK
                stringRedisTemplate.opsForStream().acknowledge("s1", "g1", record.getId());
            } catch (Exception e) {
                log.error("处理pendding订单异常", e);
                try{
                    Thread.sleep(20);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }
    }



    //    redis脚本
    private static final DefaultRedisScript<Long> SECKILL_SCRIPT;
    static{
//        静态代码块，随类一起加载执行,防止浪费IO资源
        SECKILL_SCRIPT = new DefaultRedisScript<>();
        SECKILL_SCRIPT.setLocation(new ClassPathResource("seckill.lua"));
        SECKILL_SCRIPT.setResultType(Long.class);
    }

    @Override
    public Result seckillVoucher(Long voucherId) {
        /**
        * @Description: 优惠卷秒杀
        * @Params: [voucherId]
        * @Return com.hmdp.dto.Result
        */
        Long id = UserHolder.getUser().getId();
        long order_id = redisIdWorker.nextId("order:");
//        调用脚本
        Long re = stringRedisTemplate.execute(
                SECKILL_SCRIPT,
                Collections.emptyList(),
                voucherId.toString(),
                id.toString(),
                String.valueOf(order_id)
        );
//        脚本里已经发送消息了
        int r = re.intValue();

        if(r != 0){
            return Result.fail(r == 1 ? "库存不足":"已经购买，请勿重复下单");
        }
        o = (IVoucherOrderService) AopContext.currentProxy();

        return Result.ok(order_id);

//        SeckillVoucher voucher = iSeckillVoucherService.getById(voucherId);
//        if(voucher == null){
//            throw new RuntimeException("优惠券不存在!");
//        }
//        Integer stock = voucher.getStock();
////      判断是否处于秒杀时间段
//        if(voucher.getBeginTime().isAfter(LocalDateTime.now())){
//            return Result.fail("秒杀未开始");
//        }
//        if(voucher.getEndTime().isBefore(LocalDateTime.now())){
//            return Result.fail("秒杀已结束");
//        }
////      判断是否售空
//        if(stock < 1){
//            return Result.fail("优惠劵不足");
//        }



////            获取分布式锁对象
////        SimpleRedisLock lock = new SimpleRedisLock(stringRedisTemplate, "order:" + id);
//        RLock lock = redissonClient.getLock("lock:order:" + id);
////        获取锁
//        boolean isLock = lock.tryLock();
//        if(!isLock){
////            取锁失败
//            return Result.fail("服务器错误,请重试");
//        }
//        try{
//            IVoucherOrderService o = (IVoucherOrderService) AopContext.currentProxy();
//            return o.createVoucherOrder(voucherId);
//        }finally {
////            万一有异常，可以自动释放锁
//            lock.unlock();
//        }
    }
    @Transactional
    public void handleVoucherOrder(VoucherOrder voucherOrder){
        Long userId = voucherOrder.getUserId();

        RLock lock = redissonClient.getLock("lock:order:" + userId);
//        获取锁
        boolean isLock = lock.tryLock();
        if(!isLock){
//            取锁失败
            throw new RuntimeException("请勿重复下单");
        }
        try{
            log.info("正在下单");
            o.createVoucherOrder(voucherOrder);
        }catch(Exception ex){
           throw new RuntimeException("下单出错了");
        } finally {
//            万一有异常，可以自动释放锁
            lock.unlock();
        }
    }
    @Transactional
    public Result createVoucherOrder(VoucherOrder voucherOrder) {
        Long voucherId = voucherOrder.getVoucherId();
        Long id = voucherOrder.getUserId();
        int count = query().eq("user_id", id).eq("voucher_id", voucherId).count();
        if(count > 0){
            return Result.fail("请勿重复下单");
        }


//      优惠券数量减1
        boolean success = iSeckillVoucherService.update()
                .setSql("stock = stock - 1")
                .eq("voucher_id", voucherId)
                .gt("stock",0)
                .update();

        if(!success){
//            减1失败
            return Result.fail("优惠券不足");
        }
////      创建订单
//        VoucherOrder voucherOrder = new VoucherOrder();
////      获得唯一ID
//        long order_id = redisIdWorker.nextId("order");
//        voucherOrder.setId(order_id); // 订单id来自idWorker生成唯一ID
//        voucherOrder.setVoucherId(voucherId); // 优惠券id来自传入的id
//        voucherOrder.setUserId(id); // userid来自用户进程
//        保存order
        save(voucherOrder);

        return Result.ok(voucherOrder.getId());
    }


}
