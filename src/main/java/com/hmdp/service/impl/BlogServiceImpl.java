package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.BooleanUtil;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Blog;
import com.hmdp.entity.User;
import com.hmdp.mapper.BlogMapper;
import com.hmdp.service.IBlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.service.IUserService;
import com.hmdp.utils.RedisConstants;
import com.hmdp.utils.UserHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private IUserService userService;

    @Override
    public Result queryBlogById(Long id) {
        /**
        * @Description: 根据id查询blog
        * @Params: [id]
        * @Return com.hmdp.dto.Result
        */

        Blog blog = getById(id);

        if(blog == null){
            throw new RuntimeException("页面不存在");
        }
        Long userId = blog.getUserId();
//      查询博文发布者的信息
        User user = userService.getById(userId);

        blog.setName(user.getNickName());
        blog.setIcon(user.getIcon());
        if(isLiked(id)){
            blog.setIsLike(true);
        }

        return Result.ok(blog);
    }

    @Override
    public void likeBlog(Long id) {
//        获取登录用户
        UserDTO user = UserHolder.getUser();
        if(user == null){
            throw new RuntimeException("请先登录");
        }
        Long userId = user.getId();
//        利用redis判断是否已点赞
        String key = RedisConstants.BLOG_LIKED_KEY + id;
//        Boolean b = stringRedisTemplate.opsForSet().isMember(key, userId.toString());
//        boolean isLiked = BooleanUtil.isTrue(b);
        boolean isLiked = isLiked(id);

        if(!isLiked){
            //        未点赞，则更新数据库
            boolean success = update().setSql("liked = liked + 1").eq("id", id).update();
            if(success){
                //        更新redis
                stringRedisTemplate.opsForZSet().add(key,userId.toString(),System.currentTimeMillis());
            }
        }else{
            //        已点赞则取消点赞
//        更新数据库
            boolean success = update().setSql("liked = liked - 1").eq("id",id).update();
            if(success){
                //        移除redis set
                stringRedisTemplate.opsForSet().remove(key,userId.toString());
            }

        }

    }

    @Override
    public boolean isLiked(Long blogId) {
        UserDTO user = UserHolder.getUser();
        if(user == null){
          return false;
        }
        Long userId =user.getId();
        Double score = stringRedisTemplate.opsForZSet().score(RedisConstants.BLOG_LIKED_KEY + blogId, userId.toString());

        return score != null;
    }

    @Override
    public Result getLikeList(Long id) {

//        根据blogid获得点赞用户的id集合
        String key = RedisConstants.BLOG_LIKED_KEY + id;
        Set<String> range = stringRedisTemplate.opsForZSet().range(key, 0, 4);

        if(range == null || range.isEmpty()){
            return Result.ok(Collections.emptyList());
        }

//      获取点赞用户前5
        List<Long> ids = range.stream().map(Long::valueOf).collect(Collectors.toList());
        List<User> users = userService.listByIds(ids);
//        将用户集合转为userdto集合
        List<UserDTO> list = users.stream().map(user -> BeanUtil.copyProperties(user, UserDTO.class))
                .collect(Collectors.toList());

        return Result.ok(list);
    }
}
