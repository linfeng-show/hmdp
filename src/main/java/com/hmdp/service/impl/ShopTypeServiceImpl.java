package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.CACHE_SHOP_TTL;
import static com.hmdp.utils.RedisConstants.CACHE_SHOP_TYPE;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public List<ShopType> getTypeList() {

        String key = CACHE_SHOP_TYPE;
        String json = redisTemplate.opsForValue().get(key);

        if(json != null){
            return JSONUtil.toList(json,ShopType.class);
        }

        List<ShopType> shopTypes = query().orderByAsc("sort").list();
        if(shopTypes == null){
            throw new RuntimeException("店铺分类为空");
        }

        String jsonStr = JSONUtil.toJsonStr(shopTypes);
        redisTemplate.opsForValue().set(key,jsonStr);
        redisTemplate.expire(key,CACHE_SHOP_TTL,TimeUnit.MINUTES);

        return shopTypes;
    }
}
