    -- 订单key
    local voucherId = ARGV[1]
    -- 用户id
    local userId = ARGV[2]
    -- 订单id
    local orderId = ARGV[3]
    -- 库存key
    local stockKey = 'seckill:stock:' .. voucherId
    -- 订单key
    local orderKey = 'seckill:order:' .. voucherId

    -- 获取优惠券stock,判断是否有库存
    if (tonumber(redis.call('get',stockKey)) < 1) then
        -- 库存不足
        return 1
    end

    --判断是否已经购买过
    if(redis.call('sismember', orderKey,userId) == 1) then
        -- 已经购买过
        return 2
    end

    -- 下单
    -- 减库存
    redis.call('incrby',stockKey,-1)
    -- 在已购买集合中添加用户id
    redis.call('sadd',orderKey,userId)

    --发送消息到消息队列
    redis.call('xadd','stream.orders','*','userId',userId,'voucherId',voucherId,'id',orderId)

    return 0