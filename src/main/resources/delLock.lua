-- 获取锁中的value
local id = redis.call('get',KEYS[1])
-- 比较该id和传入id是否一致，一致则删除
if(id == ARGV[1]) then
	-- 释放锁
	return redis.call('del',KEYS[1])
end
return 0